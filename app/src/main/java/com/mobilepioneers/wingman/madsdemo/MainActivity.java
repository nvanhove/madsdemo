package com.mobilepioneers.wingman.madsdemo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;

import com.MadsAdView.MadsAdView;
import com.adgoji.mraid.adview.AdViewCore;


public class MainActivity extends ActionBarActivity {
    private ViewGroup mainContent;
    AdViewCore.OnAdDownload onAdDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainContent = (ViewGroup)findViewById(R.id.mainContent);

        onAdDownload = new AdViewCore.OnAdDownload() {
            @Override
            public void noad(AdViewCore arg0) {
                System.out.println("noad");
            }

            @Override
            public void error(AdViewCore arg0, String arg1) {
                System.out.println("error");

            }

            @Override
            public void end(AdViewCore adView) {
                System.out.println("end");
            }

            @Override
            public void begin(AdViewCore adView) {
                System.out.println("begin");
            }
        };



        findViewById(R.id.btnDetail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd("2437012551"); // detail
            }
        });

        findViewById(R.id.btnChannels).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd("5436996195"); // channels
            }
        });

        showAd("2437012551"); // detail


    }


    private void showAd(String placementId) {
        mainContent.removeAllViews();
        //MadsAdView adView = new MadsAdView(this, "2437012551"); // detail
        //MadsAdView adView = new MadsAdView(this, "5436996195"); // channels


        MadsAdView adView = new MadsAdView(this, placementId);
        adView.setMadsAdType("inline");
        adView.setOnAdDownload(onAdDownload);
        adView.update();
        mainContent.addView(adView);
    }
}
